// Dependency
const mongoose = require('mongoose')
const timeZone = require('mongoose-timezone')
const uniqueValidator = require('mongoose-unique-validator')
const validator = require('validator')

// Get the Schema constructor
const Schema = mongoose.Schema

// Using Schema constructor, create a conversationSchema
const conversationSchema = new Schema(
  {
    participants: [
      {
        type: Schema.Types.ObjectId,
        ref: 'User',
      },
    ],
  },
  {
    timestamps: true,
    versionKey: false,
  }
)

// schemaPlugin
conversationSchema.plugin(timeZone, { paths: ['timestamps'] }, uniqueValidator)

const Conversation = mongoose.model('Conversation', conversationSchema) // Create model from the schema
module.exports = Conversation // Export model

// Dependency
const mongoose = require('./config/mongoose')
const cors = require('cors')
const bodyParser = require('body-parser')
const express = require('express')
const app = express()

// Config
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// Static files path
app.use(express.static(__dirname + '/public'))

// Home route. Currently just to make sure app is running returns hello world!.
app.get("/", function (req, res) {
  res.send("Hello World!");
});
// Router 
app.use('/api/users', require('./routes/user'))
app.use('/api/contact', require('./routes/contact'))
app.use('/api/conversations', require('./routes/conversation'))
app.use('/api/messages', require('./routes/message'))
app.use('/api/group', require('./routes/groupChat'))


// Listen server
const port = 5000
app.listen(port, () => {
  console.log(`Server is running in ${port}`)
})

// Dependency
const authUser = require('../middleware/authUser')
const conversationController = require('../controllers/conversationController')
const express = require('express')
const app = express.Router()

// Manage conversation A to B, B to A
app.get('/me', authUser, conversationController.index)
app.post('/me/store', authUser, conversationController.store)
app.get('/me/:id', authUser, conversationController.show)
app.delete('/me/:id/delete', authUser, conversationController.destroy)

module.exports = app

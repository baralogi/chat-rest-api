const Contact = require('../models/contactModel')
const User = require('../models/userModel')

exports.index = async (req, res) => {
  // Declaration
  let ownerId = req.user._id

  // Try-Catch
  try {
    const contact = await Contact.findOne({ ownerId: ownerId }).populate({
      path: 'listContacts',
      select: 'username avatar about',
    })
    res.status(200).send({
      status: res.statusCode,
      success: true,
      messages: 'Success load data!',
      contact,
    })
  } catch (error) {
    console.log(error)
    res.status(500).send({
      status: res.statusCode,
      success: false,
      messages: 'Server error!',
    })
  }
}

exports.store = async (req, res) => {
  // Declaration
  let listContacts = req.body.userId
  let ownerId = req.user._id

  // Try-Catch
  try {
    const data = await Contact.findOne({ ownerId: ownerId })
    let contactArr = data.listContacts
    let findContact = contactArr.includes(listContacts)
    if (listContacts == ownerId) {
      res.status(200).send({
        status: res.statusCode,
        success: true,
        messages: "You can't add yourself!",
      })
    } else if (findContact) {
      res.status(200).send({
        status: res.statusCode,
        success: true,
        messages: 'Contact already exists!',
      })
    } else {
      await Contact.findOneAndUpdate(
        { ownerId: ownerId },
        { $addToSet: { listContacts: listContacts } },
        { new: true, safe: true, upsert: true }
      )
      res.status(201).send({
        status: res.statusCode,
        success: true,
        messages: 'New contact created!',
      })
    }
  } catch (error) {
    console.log(error)
    res.status(400).send({
      status: res.statusCode,
      success: false,
      messages: 'Failed to add a new contact!',
    })
  }
}

exports.destroy = async (req, res) => {
  // Declaration
  let listContacts = req.body.userId
  let ownerId = req.user._id

  // Try-Catch
  try {
    const user = await Contact.findOneAndUpdate(
      { ownerId: ownerId },
      { $pull: { listContacts: listContacts } },
      { safe: true }
    )
    res.status(200).send({
      status: res.statusCode,
      success: true,
      messages: 'Contact deleted!',
    })
  } catch (error) {
    console.log(error)
    res.status(500).send({
      status: res.statusCode,
      success: false,
      messages: 'Server error!',
    })
  }
}
